import {
  Component,
  OnInit,
  Input,
  ElementRef,
  Inject,
  AfterViewInit,
  EventEmitter,
  Output,
  ViewChildren,
  QueryList
} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Subject } from 'rxjs';

import { SkillNode } from '../../classes/skill-node';

@Component({
  selector: 'app-skill',
  templateUrl: './skill.component.html',
  styleUrls: ['./skill.component.scss']
})
export class SkillComponent implements OnInit, AfterViewInit {

  @ViewChildren('shapes') shapes: QueryList<ElementRef>;

  @Input()
  skill: SkillNode;

  @Output()
  hasLoaded = new EventEmitter();

  @Output()
  clicked = new EventEmitter();

  // its a subject to subscribe to its value change
  canDrawLine: Subject<any> = new Subject<any>();

  constructor(@Inject(DOCUMENT) private document: any,
              private elementRef: ElementRef) { }

  ngOnInit() {

    // subscribe to canDrawLine subject value change
    this.canDrawLine.asObservable()
      .subscribe(value => {
        if (value) {
          this.drawLine();
        }
      });
  }

  ngAfterViewInit() {

    // emit to TreeComponent that this skill has been loaded
    this.hasLoaded.emit(this.skill.id);
  }

  /**
   * draw a line between the current skill HTML element and its all directChildren
   */
  drawLine() {
    const skillElement = this.elementRef.nativeElement.firstChild;
    let childSkillElement;
    let line;
    for (let i = 0 ; i < this.skill.directChildren.length ; i++) {
      childSkillElement = this.document.querySelector(`.Skill__${this.skill['directChildren'][i]}`);
      const x1 = skillElement.offsetLeft + skillElement.clientWidth;
      const y1 = skillElement.offsetTop + (skillElement.offsetHeight / 2);
      const x2 = childSkillElement.offsetLeft;
      const y2 = childSkillElement.offsetTop + (childSkillElement.offsetHeight / 2);
      line = this.elementRef.nativeElement.querySelectorAll('line')[i];
      line.setAttribute('x1', x1 + 2);
      line.setAttribute('y1', y1);
      line.setAttribute('x2', x2);
      line.setAttribute('y2', y2);
    }
  }

  /**
   * emit skill to tree component
   */
  onSkillClicked() {
    this.clicked.emit(this.skill);
  }
}

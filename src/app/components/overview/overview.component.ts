import { Component, OnInit } from '@angular/core';

import { TreeService } from '../../services/tree.service';
import { SkillTree } from '../../classes/skill-tree';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {

  trees: Array<SkillTree> = [];

  constructor(private treeService: TreeService) { }

  ngOnInit() {
    this.treeService.getTrees()
      .subscribe(res => {
        const firstTree = new SkillTree(res['firstTree']);
        const secondTree = new SkillTree(res['secondTree']);
        this.trees = [firstTree, secondTree];
      });
  }
}

import { Component, OnInit, Input, ViewChildren, QueryList, AfterViewInit, ViewChild } from '@angular/core';

import { trigger, transition, style, animate, query, stagger, animateChild } from '@angular/animations';
import { intersection } from 'lodash';

import { SkillTree } from '../../classes/skill-tree';
import { SkillComponent } from '../skill/skill.component';
import { SkillModalComponent } from '../skill-modal/skill-modal.component';

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.scss'],
  animations: [
    trigger('items', [
      transition(':enter', [
        style({ transform: 'scale(0.2)', opacity: 0 }),
        animate('1s cubic-bezier(.8, -0.6, 0.2, 1.5)',
          style({ transform: 'scale(1)', opacity: 1 }))
      ])
    ]),
    trigger('list', [
      transition(':enter', [
        query('@items', stagger(300, animateChild()))
      ]),
    ])
  ]
})
export class TreeComponent implements OnInit, AfterViewInit {

  @ViewChildren('skills') skills: QueryList<SkillComponent>;
  @ViewChild('skillModal') skillModal: SkillModalComponent;
  @Input()
  tree: SkillTree;

  treeGroups = [];
  loadedSkills = [];

  constructor() { }

  ngAfterViewInit() {
    this.initLines();
  }

  ngOnInit() {

    // get skills based on dependents
    this.treeGroups = this.tree.getSkillsByDependent();
  }

  // handle loaded skill
  handleLoadedSkill(skillId) {
    if (!this.loadedSkills.includes(skillId)) {

      // push loaded skill to loadedSKills array
      this.loadedSkills.push(skillId);
    }
  }

  // check if this skill has loaded its direct children
  hasLoadedSkillChildren(skill) {
    return skill.directChildren.length > 0 && intersection(this.loadedSkills, skill.directChildren).length === skill.directChildren.length;
  }

  // draw lines in the Skill component
  initLines() {
    this.skills.forEach((skill) => {
      if (this.hasLoadedSkillChildren(skill.skill)) {
        skill.canDrawLine.next(true);
      }
    });
  }

  // open modal with the clicked skill
  onSkillClicked(skill) {
    this.skillModal.openModal(skill);
  }
}

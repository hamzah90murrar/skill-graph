import { Component, OnInit, ViewChild } from '@angular/core';

import { ModalDirective } from 'ngx-bootstrap';
import { SkillNode } from '../../classes/skill-node';

@Component({
  selector: 'app-skill-modal',
  templateUrl: './skill-modal.component.html',
  styleUrls: ['./skill-modal.component.scss']
})
export class SkillModalComponent implements OnInit {

  @ViewChild('skillModal') skillModal: ModalDirective;

  skillToShow: SkillNode;

  constructor() { }

  ngOnInit() {

  }

  // open modal containing clicked skill
  openModal(skill: SkillNode) {
    this.setActiveSkill(skill);
    this.skillModal.show();
  }

  // set active skill to clicked skill
  setActiveSkill(skill) {
    this.skillToShow = skill;
    this.skillToShow.children = this.skillToShow.parent.getSkillChildren(skill);
  }

}

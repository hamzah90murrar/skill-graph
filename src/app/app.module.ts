import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient } from '@angular/common/http';

// 3rd party modules
import { ModalModule } from 'ngx-bootstrap/modal';

import { AppComponent } from './app.component';
import { TreeService } from './services/tree.service';
import { OverviewComponent } from './components/overview/overview.component';
import { TreeComponent } from './components/tree/tree.component';
import { SkillComponent } from './components/skill/skill.component';
import { SkillModalComponent } from './components/skill-modal/skill-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    OverviewComponent,
    TreeComponent,
    SkillComponent,
    SkillModalComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ModalModule.forRoot()
  ],
  providers: [
    TreeService,
    HttpClient
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }

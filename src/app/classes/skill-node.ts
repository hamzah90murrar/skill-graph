import { filter, includes } from 'lodash';
import { SkillTree } from './skill-tree';

export class SkillNode {

  parent: SkillTree;
  id;
  name;
  _isLocked = true;
  dependencies: Array<SkillNode> = [];
  directChildren: Array<number> = [];
  children: Array<SkillNode> = [];

  constructor(parent, id, name, isLocked, dependencies, directChildren) {
    this.parent = parent;
    this.id = id;
    this.name = name;
    this._isLocked = isLocked;
    this.dependencies = dependencies;
    this.directChildren = directChildren;
  }

  lock(): void {
    this._isLocked = true;
    for (const child of this.directChildren) {
      this.parent.lockSkillById(child);
    }
  }

  isLocked(): boolean {
    return this._isLocked;
  }

  isUnlocked(): boolean {
    return !this._isLocked;
  }

  unLock(): void {
    if (this.canBeUnlocked()) {
      this._isLocked = false;
    }

    return;
  }

  canBeUnlocked(): boolean {
    for (const dependency of this.dependencies) {
      if (dependency._isLocked) {
        return false;
      }
    }

    return true;
  }
}

import { filter, includes, find, sortBy, chain, map } from 'lodash';
import { SkillNode } from './skill-node';

export class SkillTree {

  public skills: Array<SkillNode> = [];
  public root: SkillNode;

  constructor(treeArray: Array<any>) {
    this.initTree(treeArray);
  }

  /**
   * sort nodes by id
   * and add each node to the skills array
   * @param treeArray
   */
  private initTree(treeArray: Array<any>) {
    treeArray = sortBy(treeArray);
    for (const skill of treeArray) {
      this.addSkill(skill);
    }
  }

  /**
   * add skill to the tree skills/nodes
   * @param skillNode
   */
  private addSkill(skillNode: SkillNode) {

    // get dependencies as SkillNode object
    const nodeDependencies = filter(this.skills, (dependency) => {
      return includes(skillNode.dependencies, dependency.id);
    });

    const node = new SkillNode(this, skillNode.id, skillNode.name, skillNode.isLocked, nodeDependencies, skillNode.directChildren);
    if (!this.root) {
      this.root = node;
    }

    this.skills.push(node);
  }

  /**
   * generate the children array of skill node
   * @param {SkillNode} skillNode
   * @returns {Array<SkillNode>}
   */
  getSkillChildren(skillNode: SkillNode): Array<SkillNode> {
    const children: Array<SkillNode> = [];
    for (const skill of this.skills) {
      for (const dependent of skill.dependencies) {
        if (dependent.id === skillNode.id) {
          children.push(skill);
        }
      }
    }

    return children;
  }

  lockSkillById(id) {
    const skill = find(this.skills, (entry) => {
      return entry.id === id;
    });

    if (skill) {
      skill.lock();
    }
  }

  /**
   * get skills grouped by dependents
   */
  getSkillsByDependent() {
  return sortBy(chain(this.getSkills())
    .groupBy((entry) => {
      return entry.dependencies.length;
    })
      .map((value, key) => {
        return {
          key: +key,
          entries: value
        };
      })
      .value(), 'key');
  }

  /**
   * get tree skills
   */
  getSkills() {
    return this.skills;
  }
}
